<?php

namespace App\Jobs;

use App\Models\Ticket;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StoreTickets implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;
    private $provider_id;

    /**
     * @param array $data
     * @param int $provider_id
     */
    public function __construct(array $data, int $provider_id)
    {
        $this->data = $data;
        $this->provider_id = $provider_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->data as $row) {
            Ticket::query()->updateOrCreate([
                'provider_id' => $this->provider_id,
                'ticket_id'   => $row['ticket_id']
            ], $row);
        }
    }

}
