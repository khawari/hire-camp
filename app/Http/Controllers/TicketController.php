<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    public function tickets()
    {
        $tickets = Ticket::query()->paginate(15);

        return view('pages.tickets.index', compact('tickets'));

    }
}
