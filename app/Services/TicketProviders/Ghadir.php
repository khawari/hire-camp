<?php

namespace App\Services\TicketProviders;

use Illuminate\Support\Facades\Http;

class Ghadir implements ProviderInterface
{
    private $url;

    public function __construct($url)
    {
        $this->url = $url;
    }

    public function data(): array
    {
        return $this->format();
    }

    public function fetch(): array
    {
        return Http::get($this->url)->collect()->toArray();
    }

    public function format(): array
    {
        $items = $this->fetch();
        $data = [];

        foreach ($items as $item) {
            $data[] = [
                'ticket_id'     => $item['ticket_id'],
                'origin'        => $item['origin'],
                'destination'   => $item['destination'],
                'date'          => $item['date'],
                'flight_number' => $item['flight_number'],
                'airline'       => $item['airline'],
                'price'         => $item['price'],
                'count'         => $item['count'],
            ];
        }

        return $data;
    }

}
