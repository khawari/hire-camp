<?php

namespace App\Services\TicketProviders;

use Illuminate\Support\Facades\Http;

class Raja implements ProviderInterface
{
    private $url;

    public function __construct($url)
    {
        $this->url = $url;
    }

    public function data(): array
    {
        return $this->format();
    }

    public function fetch(): array
    {
        return Http::get($this->url)->collect()->toArray();
    }

    public function format(): array
    {
        $items = $this->fetch();
        $data = [];

        foreach ($items as $item) {
            $data[] = [
                'ticket_id'     => $item['TicketID'],
                'origin'        => $item['Origin'],
                'destination'   => $item['Destination'],
                'date'          => $item['FlightDate'],
                'flight_number' => $item['FlightNumber'],
                'airline'       => $item['Airline'],
                'price'         => $item['Price'],
                'count'         => $item['Count'],
            ];
        }

        return $data;
    }

}
