<?php

namespace App\Services\TicketProviders;

use App\Jobs\StoreTickets;

class TicketServiceProvider
{
    private $provider;
    public $url;
    public $provider_id;

    public function __construct(ProviderInterface $provider, $provider_id)
    {
        $this->provider = $provider;
        $this->provider_id = $provider_id;
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    public function handle()
    {
        $data = $this->provider->data();

        foreach (array_chunk($data, 500) as $chunk_data) {
            dispatch(new StoreTickets($chunk_data, $this->provider_id))->onQueue('high');
        }
    }

}
