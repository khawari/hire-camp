<?php

namespace App\Services\TicketProviders;

interface ProviderInterface
{
    /**
     * Return data from provider
     *
     * @return array
     */
    public function data(): array;

    /**
     * Call http request
     *
     * @return array
     */
    public function fetch(): array;

    /**
     * format data
     *
     * @return array
     */
    public function format(): array;
}
