<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;

    protected $table = 'tickets';
    protected $fillable = ['provider_id', 'ticket_id', 'origin', 'destination', 'date', 'airline', 'price', 'count'];


    public function provider()
    {
        return $this->belongsTo(TicketProvider::class, 'provider_id', 'id');
    }

}
