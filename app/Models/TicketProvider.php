<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class TicketProvider extends Model
{
    use HasFactory;

    protected $table = 'ticket_providers';
    protected $fillable = ['name', 'api_url', 'api_key', 'api_secret', 'order', 'duration', 'status'];

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class, 'provider_id', 'id');
    }

}
