<?php

namespace App\Console\Commands;

use App\Models\TicketProvider;
use App\Services\TicketProviders\TicketServiceProvider;
use Illuminate\Console\Command;

class FetchTicketProvider extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ticket_providers:fetch {--id=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch data from all or specific provider with --id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $providers = $this->getTicketProvider();

        foreach ($providers as $provider) {

            $provider_class_name = "\App\Services\TicketProviders\\" . $provider->name;

            if (!class_exists($provider_class_name)) {
                continue;
            }

            $ticket_provider = new TicketServiceProvider(new $provider_class_name($provider->api_url), $provider->id);
            $ticket_provider->handle();

            $this->info($provider->name . " ticket providers fetched successfully at " . date('Y-m-d H:i:s'));
        }
    }

    private function getTicketProvider()
    {
        if (empty($this->option('id'))) {
            $providers = TicketProvider::query()->orderBy('order', 'desc')->get();
        } else {
            $id = intval($this->option('id'));
            $providers = TicketProvider::query()->where('id', $id)->orderBy('order', 'desc')->get();
        }

        return $providers;
    }

}
