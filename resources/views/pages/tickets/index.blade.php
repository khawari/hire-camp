<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-12">
            <table class="table table-bordered">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Ticket ID</th>
                        <th scope="col">Origin</th>
                        <th scope="col">Destination</th>
                        <th scope="col">Price</th>
                        <th scope="col">Count</th>
                        <th scope="col">Provider</th>
                    </tr>
                    </thead>
                    <tbody class="table-group-divider">

                    @foreach($tickets as $ticket)
                        <tr>
                            <td>{{ $ticket->ticket_id }}</td>
                            <td>{{ $ticket->origin }}</td>
                            <td>{{ $ticket->destination }}</td>
                            <td>{{ $ticket->price }}</td>
                            <td>{{ $ticket->count }}</td>
                            <td>{{ $ticket->provider->name }}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            {{ $tickets->links() }}
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>



