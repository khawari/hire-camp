<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_providers', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();
            $table->string('api_url');
            $table->string('api_key');
            $table->string('api_secret');
            $table->integer('order');
            $table->integer('duration')->default(60);
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_providers');
    }
}
