<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('provider_id')->unsigned();
            $table->foreign('provider_id')->references('id')->on('ticket_providers')->onDelete('restrict')->onUpdate('cascade');

            $table->bigInteger('ticket_id')->unsigned()->index()->unique();
            $table->string('origin');
            $table->string('destination');
            $table->string('date');
            $table->string('airline');
            $table->double('price')->unsigned();
            $table->integer('count')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
